import socket
HOST = 'localhost'
PORT = 50000
tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
dest = (HOST, PORT)
tcp.connect(dest)
string = input('Digite algo:')
tcp.send(str.encode(string))
invertida = tcp.recv(1024)
print('palavra invertida:', invertida.decode())
